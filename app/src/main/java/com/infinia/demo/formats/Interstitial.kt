package com.infinia.demo.formats

import android.content.Context
import android.util.Log
import com.sdk.infinia.adserver.ui.InfiniaInterstitialManager

class Interstitial(ctx: Context) : InfiniaInterstitialManager(ctx) {

    companion object { private val TAG = "Interstitial" }

    init {
        infiniaInterstitialListener = object : InfiniaInterstitialManager.InfiniaInterstitialListener {
            override fun onInfiniaInterstitialAdLoaded() {
                Log.i(TAG,"Interstitial loaded")
            }

            override fun onInfiniaInterstitialAdFailedToLoad() {
                Log.i(TAG, "Interstitial loading failed")
            }

            override fun onInfiniaInterstitialAdShown() {
                Log.i(TAG, "Interstitial was shown")
            }

            override fun onInfiniaInterstitialAdFailedToShow() {
                Log.i(TAG, "Interstitial failed to show")
            }

            override fun onInfiniaInterstitialAdClicked() {
                Log.i(TAG, "Interstitial was clicked")
            }

            override fun onInfiniaInterstitialAdDismissed() {
                Log.i(TAG, "Interstitial was dismissed")
            }

            override fun onInfiniaInterstitialAdVideoEvent() {
                Log.i(TAG, "Video event was triggered on Interstitial")

            }

            override fun onInfiniaInterstitialAdDisabled() {
                Log.i(TAG, "Interstitial is disabled")

            }

        }
    }
}