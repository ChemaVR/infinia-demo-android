package com.infinia.demo.formats

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import com.infinia.demo.R
import com.sdk.infinia.adserver.format.InfiniaAdView

class Banner: InfiniaAdView {

    private lateinit var listener: InfiniaAdViewListener
    private var ctx: Context

    companion object { private val TAG = "Banner" }

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)

    init {
        this.ctx = context
    }

    fun showBanner() {

        listener = object : InfiniaAdViewListener {

            override fun onInfiniaAdViewLoaded(infiniaAdView: InfiniaAdView) {
                Log.i(TAG, "Banner loading completed")
            }

            override fun onInfiniaAdViewFailedToLoad(infiniaAdView: InfiniaAdView, e: Exception) {
                Log.i(TAG, "Banner loading failed: " + e.message)
            }

            override fun onInfiniaAdViewClicked(infiniaAdView: InfiniaAdView) {
                Log.i(TAG, "Banner was clicked")
            }

            override fun onInfiniaAdViewExpanded(infiniaAdView: InfiniaAdView) {
                Log.i(TAG, "Banner was expanded")

            }

            override fun onInfiniaAdViewCollapsed(infiniaAdView: InfiniaAdView) {
                Log.i(TAG, "Banner was collapsed")
            }

            override fun onInfiniaAdViewResized(infiniaAdView: InfiniaAdView) {
                Log.i(TAG, "Banner was resized")

            }

            override fun onInfiniaAdViewClosed(infiniaAdView: InfiniaAdView) {
                Log.i(TAG, "Banner was closed")
            }

            override fun onInfiniaAdViewVideoEvent(infiniaAdView: InfiniaAdView, var2: Int) {
                Log.i(TAG, "Video event $infiniaAdView was triggered on Banner")

            }

            override fun onInfiniaAdViewDisabled() {
                Log.i(TAG, "Banner was disabled")

            }
        }

        infiniaAdViewListener = listener

        loadBanner(context, R.color.colorLoaderBackground)
    }

    fun showMpu(isMpu2: Boolean) {

        val listener = object : InfiniaAdViewListener {

            override fun onInfiniaAdViewLoaded(var1: InfiniaAdView) {
                Log.i(TAG, "Mpu loading completed")
            }

            override fun onInfiniaAdViewFailedToLoad(var1: InfiniaAdView, e: Exception) {
                Log.i(TAG, "Mpu loading failed: " + e.message)
            }

            override fun onInfiniaAdViewClicked(var1: InfiniaAdView) {
                Log.i(TAG, "Mpu was clicked")
            }

            override fun onInfiniaAdViewExpanded(var1: InfiniaAdView) {
                Log.i(TAG, "Mpu was expanded")
            }

            override fun onInfiniaAdViewCollapsed(var1: InfiniaAdView) {
                Log.i(TAG, "Mpu was collapsed")
            }

            override fun onInfiniaAdViewResized(var1: InfiniaAdView) {
                Log.i(TAG, "Mpu was resized")
            }

            override fun onInfiniaAdViewClosed(var1: InfiniaAdView) {
                Log.i(TAG, "Mpu was closed")
            }

            override fun onInfiniaAdViewVideoEvent(videoEvent: InfiniaAdView, var2: Int) {
                Log.i(TAG, "Video event $videoEvent was triggered on Mpu")
            }

            override fun onInfiniaAdViewDisabled() {
                Log.i(TAG, "Mpu was disabled")
            }
        }

        infiniaAdViewListener = listener

      when(isMpu2) {
          true -> loadMpu2(ctx)
          false -> loadMpu(ctx)
      }
    }

    fun showVideoBanner () {

        listener = object : InfiniaAdViewListener {

            override fun onInfiniaAdViewLoaded(var1: InfiniaAdView) {
                Log.i(TAG, "Video Banner loading completed")
            }

            override fun onInfiniaAdViewFailedToLoad(var1: InfiniaAdView, e: Exception) {
                Log.i(TAG, "Video Banner loading failed: " + e.message)
            }

            override fun onInfiniaAdViewClicked(var1: InfiniaAdView) {
                Log.i(TAG, "Video Banner was clicked")
            }

            override fun onInfiniaAdViewExpanded(var1: InfiniaAdView) {
                Log.i(TAG, "Video Banner was expanded")
            }

            override fun onInfiniaAdViewCollapsed(var1: InfiniaAdView) {
                Log.i(TAG, "Video Banner was collapsed")
            }

            override fun onInfiniaAdViewResized(var1: InfiniaAdView) {
                Log.i(TAG, "Video Banner was resized")
            }

            override fun onInfiniaAdViewClosed(var1: InfiniaAdView) {
                Log.i(TAG, "Video Banner was closed")
            }

            override fun onInfiniaAdViewVideoEvent(var1: InfiniaAdView, videoEvent: Int) {
                Log.i(TAG, "Video event $videoEvent was triggered on VideoOutstream")
            }

            override fun onInfiniaAdViewDisabled() {
                Log.i(TAG, "Video Banner was disabled")
            }
        }

        infiniaAdViewListener = listener
        loadVideoOutstream(ctx)

    }

}
