package com.infinia.demo.formats

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.sdk.infinia.SdkBuildConfig
import com.smartadserver.android.library.model.SASAdElement
import com.smartadserver.android.library.model.SASAdPlacement
import com.smartadserver.android.library.ui.SASBannerView
import com.smartadserver.android.library.ui.SASRotatingImageLoader

class Cuopon : SASBannerView {

    companion object {
        val siteID = 292028L
        val pageID = 1166965L
        val formatID = 64788L
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)

    private var adListener: BannerListener? = null

    fun loadBanner(ctx: Context, colorLoaderBackground: Int) {
        this.visibility = View.VISIBLE
        val loader = SASRotatingImageLoader(ctx)
        loader.setBackgroundColor(resources.getColor(colorLoaderBackground))
        loaderView = loader
        load(ctx, "banner")
    }

    fun loadMpu(ctx: Context) {
        load(ctx, "mpu")
    }

    private fun load(ctx: Context, format: String) {
        try {
                adListener = object : BannerListener {
                    override fun onBannerAdLoaded(adView: SASBannerView, adElement: SASAdElement) {
                        Log.i(SdkBuildConfig.LOG_TAG + "_load", "$format loading completed")
                    }

                    override fun onBannerAdFailedToLoad(adView: SASBannerView, e: Exception) {
                        Log.i(SdkBuildConfig.LOG_TAG + "_load", format + " loading failed: " + e.message)
                    }

                    override fun onBannerAdClicked(adView: SASBannerView) {
                        Log.i(SdkBuildConfig.LOG_TAG + "_load", "$format was clicked")

                    }

                    override fun onBannerAdExpanded(adView: SASBannerView) {
                        Log.i(SdkBuildConfig.LOG_TAG + "_load", "$format was expanded")
                    }

                    override fun onBannerAdCollapsed(adView: SASBannerView) {
                        Log.i(SdkBuildConfig.LOG_TAG + "_load", "$format was collapsed")
                    }

                    override fun onBannerAdResized(adView: SASBannerView) {
                        Log.i(SdkBuildConfig.LOG_TAG + "_load", "$format was resized")
                    }

                    override fun onBannerAdClosed(adView: SASBannerView) {
                        Log.i(SdkBuildConfig.LOG_TAG + "_load", format + "was closed")
                    }

                    override fun onBannerAdVideoEvent(adView: SASBannerView, videoEvent: Int) {
                        Log.i(SdkBuildConfig.LOG_TAG + "_load", "Video event $videoEvent was triggered on $format")
                    }
                }

                loadAd(SASAdPlacement(siteID, pageID, formatID, "", true))


        } catch (e: Exception) {
            Log.e(SdkBuildConfig.LOG_TAG + "_load", e.message)
        }

    }


    fun on_destroy() {
        super.onDestroy()
    }

}