package com.infinia.demo.utils

import androidx.core.content.res.ResourcesCompat
import com.infinia.demo.R
import com.nightonke.boommenu.BoomButtons.HamButton


object BoomBuilderManager {

    private val iconFormatResources = intArrayOf(R.drawable.inter, R.drawable.banner, R.drawable.robadoble, R.drawable.roba, R.drawable.video_instream, R.drawable.video_read, R.drawable.video_read)

    private val titleResources = intArrayOf(R.string.txt_title_format_interstitial, R.string.txt_title_format_banner, R.string.txt_title_format_mpu2, R.string.txt_title_format_mpu, R.string.txt_title_format_video_instream, R.string.txt_title_format_video_read, R.string.txt_title_format_coupon)

    private val subtitleResources = intArrayOf(R.string.txt_size_format_interstitial, R.string.txt_size_format_banner, R.string.txt_size_format_mpu2, R.string.txt_size_format_mpu, R.string.txt_size_format_video_instream, R.string.txt_size_format_video_banner, R.string.txt_size_format_coupon )

    private var iconFormatIndex = 0

    private var titleResourceIndex = 0

    private var subtitleResourceIndex = 0

    val hamButtonBuilder: HamButton.Builder
        get() = HamButton.Builder()
                .normalImageRes(getIconFormatResources())
                .normalTextRes(getTitleResources())
                .subNormalTextRes(getSubTitleResources())

    internal fun getIconFormatResources(): Int {
        if (iconFormatIndex >= iconFormatResources.size) iconFormatIndex = 0
        return iconFormatResources[iconFormatIndex++]
    }

    internal fun getTitleResources(): Int {
        if (titleResourceIndex >= titleResources.size) titleResourceIndex = 0
        return titleResources[titleResourceIndex++]
    }

    internal fun getSubTitleResources(): Int {
        if (subtitleResourceIndex >= subtitleResources.size) subtitleResourceIndex = 0
        return subtitleResources[subtitleResourceIndex++]
    }

}
