package com.infinia.demo.utils

import android.graphics.Point
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity

class VideoInstreamUtils {
    companion object {
        fun addRule(params: ViewGroup.LayoutParams, subject: Int) {
            (params as RelativeLayout.LayoutParams).addRule(RelativeLayout.BELOW, subject)
        }

        fun modifyDimensions(params: ViewGroup.LayoutParams, height: Int, width: Int = RelativeLayout.LayoutParams.MATCH_PARENT) {
            params.height = height
            params.width = width
        }

        fun setMaxHeightFromScreen(activity: AppCompatActivity): Int {
            val display = activity.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            return size.y
        }

        fun modifyMargin(params: ViewGroup.LayoutParams, margin: Int) {
            (params as RelativeLayout.LayoutParams).setMargins(margin, margin, margin, margin)
        }
    }
}