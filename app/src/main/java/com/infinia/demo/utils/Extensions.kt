package com.infinia.demo.utils


import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.infinia.demo.R

fun AppCompatActivity.setupActionBar(displayHomeEnabled: Boolean, toolbar: Toolbar?) {
    setSupportActionBar(toolbar)
    supportActionBar?.setDisplayShowTitleEnabled(false)
    supportActionBar?.let {
        title = getString(R.string.txt_title_toolbar)
        when (displayHomeEnabled) {
            true -> { it.setDisplayHomeAsUpEnabled(true)
                      it.setDisplayHomeAsUpEnabled(true) }
        }
    }
}