package com.infinia.demo.presentation

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.infinia.demo.R
import com.infinia.demo.formats.Banner
import com.infinia.demo.utils.setupActionBar

class LoadInfiniaMpuActivity : BaseActivity() {

    private lateinit var mpu: Banner
    private var isMpu2: Boolean? = null
    private lateinit var toolbar: Toolbar

    /**
     * performs Activity initialization after creation
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        when(isMpu2()){
            true ->  setContentView(R.layout.activity_mpu2)
            false -> setContentView(R.layout.activity_mpu)
        }

        setupViews()
        setupActionBar(true, toolbar)
        showMpu()
    }

    private fun isMpu2(): Boolean {
        isMpu2 = intent.getBooleanExtra("isMpu2", false)
        return isMpu2 as Boolean
    }
    private fun showMpu() {
        isMpu2?.let { mpu.showMpu(it) }
    }

    override fun onDestroy() {
        super.onDestroy()
        mpu.on_destroy()
    }

    private fun setupViews() {
        mpu = findViewById(R.id.infiniaMpu)
        toolbar = findViewById(R.id.toolbar)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}

