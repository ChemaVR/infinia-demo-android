package com.infinia.demo.presentation

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.infinia.demo.R
import com.infinia.demo.formats.Interstitial
import com.infinia.demo.utils.setupActionBar

class LoadInfiniaInterstitialActivity : BaseActivity() {

    private lateinit var toolbar: Toolbar
    private var interstitial: Interstitial? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_interstitial)
        setupViews()
        setupActionBar(true, toolbar)
        interstitial = Interstitial(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        interstitial?.on_destroy()
    }

    private fun setupViews() {
        toolbar = findViewById(R.id.toolbar)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }



}