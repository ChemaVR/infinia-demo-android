package com.infinia.demo.presentation

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.infinia.demo.R
import com.infinia.demo.formats.Banner
import com.infinia.demo.utils.setupActionBar

class LoadInfiniaVideoBannerActivity : BaseActivity() {

    private lateinit var videoBanner: Banner
    private lateinit var toolbar: Toolbar

    /**
     * performs Activity initialization after creation
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_banner)
        setupViews()
        setupActionBar(true, toolbar)
        videoBanner.showVideoBanner()

    }
    override fun onDestroy() {
        super.onDestroy()
        videoBanner.on_destroy()
    }


    private fun setupViews() {
        videoBanner = findViewById(R.id.infiniaBannerVideo)
        toolbar = findViewById(R.id.toolbar)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
