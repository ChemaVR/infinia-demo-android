package com.infinia.demo.presentation

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.infinia.demo.R
import com.infinia.demo.formats.Banner
import com.infinia.demo.utils.setupActionBar

class LoadInfiniaBannerActivity : BaseActivity() {

    private lateinit var banner: Banner
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_banner)
        setupViews()
        setupActionBar(true, toolbar)
        banner.showBanner()
    }

    override fun onDestroy() {
        super.onDestroy()
        banner.on_destroy()
    }

    private fun setupViews() {
        banner = findViewById(R.id.infiniaBanner)
        toolbar = findViewById(R.id.toolbar)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}