package com.infinia.demo.presentation

import android.content.res.Configuration
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.infinia.demo.R
import com.infinia.demo.utils.VideoInstreamUtils
import com.infinia.demo.utils.setupActionBar
import com.sdk.infinia.adserver.format.InfiniaVideoInstream



class LoadInfiniaVideoInstreamActivity : BaseActivity() {

    companion object {
        //static final private String CONTENT_VIDEO_URL = "https://s3-eu-west-1.amazonaws.com/infinia-vid-stream/this_is_us_nbc_trailer.mp4";
        private val CONTENT_VIDEO_URL = "https://ns.sascdn.com/mobilesdk/samples/videos/BigBuckBunnyTrailer_360p.mp4"
    }


    private lateinit var infiniaVideoInstream: InfiniaVideoInstream
    private lateinit var toolbar: Toolbar
    private lateinit var title: TextView
    private lateinit var subTitle: TextView
    private lateinit var body: TextView
    private lateinit var player: RelativeLayout
    private lateinit var content_player_container: FrameLayout
    private lateinit var simpleExoPlayerView: SimpleExoPlayerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_instream)
        setupViews()
        setupActionBar(true, toolbar)

        infiniaVideoInstream = InfiniaVideoInstream.Builder(this)
                .setUrl(CONTENT_VIDEO_URL)
                .setForceSkipDelay(true)
                .setSkipDelay(5000)
                .hasPrerollData(true)
                .hasMidrollData(true)
                .hasPostrollData(false)
                .setRepeatMode(InfiniaVideoInstream.REPEAT_MODE_ONE)
                .create()
    }

    override fun onResume() {
        super.onResume()
        infiniaVideoInstream.on_resume()

    }

    override fun onPause() {
        super.onPause()
        infiniaVideoInstream.on_pause()
    }

    override fun onDestroy() {
        super.onDestroy()
        infiniaVideoInstream.on_destroy()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        infiniaVideoInstream.onConfigurationChanged(newConfig)
        modifyViews(newConfig.orientation)

    }

    private fun setupViews() {
        toolbar = findViewById(R.id.toolbar)
        title = findViewById(R.id.title)
        subTitle = findViewById(R.id.subtitle)
        body = findViewById(R.id.body)
        player = findViewById(R.id.player)
        content_player_container = findViewById(R.id.content_player_container)
        simpleExoPlayerView = findViewById(R.id.simple_exo_player_view)
    }

    private fun modifyViews(orientation: Int) {
        val params = player.layoutParams
        when(orientation) {
            ORIENTATION_LANDSCAPE -> {
                visibilityViews(GONE)
                VideoInstreamUtils.modifyDimensions(params, VideoInstreamUtils.setMaxHeightFromScreen(this))
                VideoInstreamUtils.modifyMargin(params, 0)
                player.setLayoutParams(params)
            }

            ORIENTATION_PORTRAIT -> {
                visibilityViews(VISIBLE)
                VideoInstreamUtils.addRule(params, R.id.subtitle)
                VideoInstreamUtils.modifyDimensions(params, 770)
                VideoInstreamUtils.modifyMargin(params, 42)
                player.setLayoutParams(params)
            }

        }

    }

    private fun visibilityViews(visibility: Int) {
        title.visibility = visibility
        subTitle.visibility = visibility
        body.visibility = visibility
        toolbar.visibility = visibility
    }
}
