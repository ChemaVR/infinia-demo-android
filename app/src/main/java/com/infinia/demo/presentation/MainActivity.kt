package com.infinia.demo.presentation

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.collection.ArrayMap
import com.crashlytics.android.Crashlytics
import com.infinia.demo.MyApplication
import com.infinia.demo.R
import com.infinia.demo.utils.BoomBuilderManager
import com.infinia.demo.utils.setupActionBar
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum
import com.nightonke.boommenu.BoomMenuButton
import com.nightonke.boommenu.ButtonEnum
import com.nightonke.boommenu.Piece.PiecePlaceEnum
import com.sdk.infinia.event.InfiniaAppEventParameterName
import com.sdk.infinia.event.InfiniaAppEventType
import com.sdk.infinia.ui.Consent
import com.sdk.infinia.util.InfiniaManager
import io.fabric.sdk.android.Fabric


class MainActivity : AppCompatActivity() {

    private lateinit var infiniaManager: InfiniaManager
    private lateinit var bmb: BoomMenuButton
    private lateinit var toolbar: Toolbar
    private var params: ArrayMap<String, String> = ArrayMap<String, String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initCrashlytics()
        infiniaManager = MyApplication.infiniaManager!!
        setupViews()
        setupActionBar(false, toolbar)
        setupConsent()
        setupBoomBuilder()

    }

    private fun initCrashlytics() {
        Fabric.with(this, Crashlytics())
    }

    private fun sendEvent(params: ArrayMap<String, String>, typeEvent: String) {
            infiniaManager.trackEvent(params, typeEvent)
    }

    private fun setupConsent() {

        val consentInfinia = Consent.Builder(this)
                .setTitleColor(Color.RED)
                .setContentColor(Color.BLACK)
                .setBackgroundColor(Color.WHITE)
                .setContentSecondLayer(com.infinia.demo.R.string.txt_content_second_layer)
                .setButtonColor(Color.BLUE)
                .buildConsent()

        infiniaManager.showInfiniaConsent(consentInfinia)

    }

    private fun goToShowAd(index: Int) {
        when (index) {
            0 -> {
                intent = Intent(this@MainActivity, LoadInfiniaInterstitialActivity::class.java)
                params.put(InfiniaAppEventParameterName.NAME, "INTERSTITIAL")
                sendEvent( params, InfiniaAppEventType.ITEM_OPENED)
            }
            1 -> {
                intent = Intent(this@MainActivity, LoadInfiniaBannerActivity::class.java)
                params.put(InfiniaAppEventParameterName.NAME, "BANNER")
                sendEvent( params, InfiniaAppEventType.ITEM_OPENED)
            }
            2 -> {
                intent = Intent(this@MainActivity, LoadInfiniaMpuActivity::class.java)
                intent?.putExtra("isMpu2", true)
                params.put(InfiniaAppEventParameterName.NAME, "MPU2")
                sendEvent( params, InfiniaAppEventType.ITEM_OPENED)
            }
            3 -> {
                intent = Intent(this@MainActivity, LoadInfiniaMpuActivity::class.java)
                params.put(InfiniaAppEventParameterName.NAME, "MPU")
                sendEvent( params, InfiniaAppEventType.ITEM_OPENED)
            }
            4 -> {
                intent = Intent(this@MainActivity, LoadInfiniaVideoInstreamActivity::class.java)
                params.put(InfiniaAppEventParameterName.NAME, "VIDEOINSTREAM")
                sendEvent( params, InfiniaAppEventType.ITEM_OPENED)
            }
            5 -> {
                intent = Intent(this@MainActivity, LoadInfiniaVideoBannerActivity::class.java)
                params.put(InfiniaAppEventParameterName.NAME, "VIDEOBANNER")
                sendEvent( params, InfiniaAppEventType.ITEM_OPENED)
            }

            6 -> {
                intent = Intent(this@MainActivity, LoadInfiniaCouponActivity::class.java)
                params.put(InfiniaAppEventParameterName.NAME, "COUPON")
                sendEvent( params, InfiniaAppEventType.ITEM_OPENED)
            }
        }

        intent?.let {startActivity(it)}

    }

    private fun setupBoomBuilder() {

        bmb.let {
            bmb.buttonEnum = ButtonEnum.Ham
            bmb.piecePlaceEnum = PiecePlaceEnum.HAM_7
            bmb.buttonPlaceEnum = ButtonPlaceEnum.HAM_7
            for (i in 0..6)
                bmb.addBuilder(BoomBuilderManager.hamButtonBuilder.listener { index -> goToShowAd(index) })
        }
    }

    private fun setupViews() {
        bmb = findViewById(R.id.bmb)
        toolbar = findViewById(R.id.toolbar)
    }
}
