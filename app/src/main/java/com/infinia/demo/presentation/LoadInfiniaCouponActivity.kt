package com.infinia.demo.presentation

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.infinia.demo.R
import com.infinia.demo.formats.Cuopon
import com.infinia.demo.utils.setupActionBar

class LoadInfiniaCouponActivity : BaseActivity() {

    private lateinit var cuopon: Cuopon
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coupon)
        setupViews()
        setupActionBar(true, toolbar)
        showCoupon()
    }

    private fun showCoupon() {
        cuopon.loadMpu(this);
    }

    override fun onDestroy() {
        super.onDestroy()
        cuopon.on_destroy()
    }

    private fun setupViews() {
        cuopon = findViewById(R.id.infiniaCoupon)
        toolbar = findViewById(R.id.toolbar)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}