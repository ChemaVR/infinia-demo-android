package com.infinia.demo

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.multidex.MultiDexApplication
import com.sdk.infinia.util.InfiniaManager



class MyApplication : MultiDexApplication(), LifecycleObserver {

    companion object{
        const val appToken = "1234"
        var infiniaManager: InfiniaManager? = null
    }

    override fun onCreate() {
        super.onCreate()
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        infiniaManager ifNull {
            infiniaManager = InfiniaManager.Builder(this)
                    .setAppToken(appToken)
                    .setDebugMode(true)
                    .init()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        infiniaManager?.background()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        infiniaManager?.foreground()
    }



    infix fun Any?.ifNull(block: () -> Unit) {
        if (this == null) block()
    }
}

